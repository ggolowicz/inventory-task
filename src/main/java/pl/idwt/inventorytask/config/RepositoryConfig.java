package pl.idwt.inventorytask.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import pl.idwt.inventorytask.validator.ItemValidator;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {

    @Autowired
    private ItemValidator itemValidator;

    @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeSave", itemValidator);
        validatingListener.addValidator("beforeCreate", itemValidator);
    }
}
