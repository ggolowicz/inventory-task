package pl.idwt.inventorytask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventoryTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(InventoryTaskApplication.class, args);
    }
}
