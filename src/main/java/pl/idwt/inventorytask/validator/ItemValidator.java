package pl.idwt.inventorytask.validator;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.idwt.inventorytask.model.Item;

@Component
public class ItemValidator implements Validator {

    private static String alnumRegex = "^[A-Za-z0-9]*$";

    @Override
    public boolean supports(Class<?> clazz) {
        return Item.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Item item = (Item) target;

        if (item.getName() == null) {
            errors.rejectValue("name", "name.null", "name is missing");
        } else {
            if(item.getName().isEmpty()){
                errors.rejectValue("name", "name.empty", "name can't be empty");
            }

            if(!item.getName().matches(alnumRegex)){
                errors.rejectValue("name", "name.alnum", "name have to be alpha-numeric");
            }

            if(item.getName().length() > 50){
                errors.rejectValue("name", "name.max", "name must not exceed 50 characters");
            }
        }


        if (item.getDescription() == null) {
            errors.rejectValue("description", "description.null", "description is missing");
        } else {
            if(item.getDescription().isEmpty()){
                errors.rejectValue("description", "description.empty", "description can't be empty");
            }

            if(!item.getDescription().matches(alnumRegex)){
                errors.rejectValue("description", "description.alnum", "description have to be alpha-numeric");
            }

            if(item.getDescription().length() > 150){
                errors.rejectValue("description", "description.max", "description must not exceed 150 characters");
            }
        }

        if(item.getCount() < 1 || item.getCount() > 100){
            errors.rejectValue("count", "count.range", "count must be between 1 and 100");
        }

        if (item.getUpdatedAt() != null) {
            errors.rejectValue("updatedAt", "updatedAt.forbidden", "setting updatedAt manually is not allowed");
        }

    }

}
