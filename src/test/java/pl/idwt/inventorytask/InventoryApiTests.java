package pl.idwt.inventorytask;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.idwt.inventorytask.model.Item;
import pl.idwt.inventorytask.model.repository.ItemRepository;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = InventoryTaskApplication.class)
@WebAppConfiguration
public class InventoryApiTests extends ApiTestsBase{

    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void before() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        this.itemRepository.deleteAll();
    }

    @Test
    public void itemNotFound() throws Exception {
        mockMvc.perform(get("/items/20"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void emptyList() throws Exception {
        mockMvc.perform(get("/items"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.page.totalElements", is(0)));
    }

    @Test
    public void testListOneElement() throws Exception {
        Item item = new Item();
        item.setName("ItemName");
        item.setDescription("ItemDesc");
        item.setCount(3);

        itemRepository.save(item);

        mockMvc.perform(get("/items"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.page.totalElements", is(1)))
                .andExpect(jsonPath("$._embedded.items[0].name", is(item.getName())))
                .andExpect(jsonPath("$._embedded.items[0].description", is(item.getDescription())))
                .andExpect(jsonPath("$._embedded.items[0].count", is(item.getCount())))
                .andExpect(jsonPath("$._embedded.items[0].updatedAt", notNullValue()));
    }

    @Test
    public void testOneElement() throws Exception {
        Item item = new Item();
        item.setName("ItemName");
        item.setDescription("ItemDesc");
        item.setCount(3);

        item = itemRepository.save(item);

        mockMvc.perform(get(String.format("/items/%d", item.getId())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is(item.getName())))
                .andExpect(jsonPath("$.description", is(item.getDescription())))
                .andExpect(jsonPath("$.count", is(item.getCount())))
                .andExpect(jsonPath("$.updatedAt", notNullValue()));
    }

    @Test
    public void testValidationMessages() throws Exception {
        Object[][] testData = new Object[][]{
                {"{ \"ne\" : \"\", \"description\" : \"ItemDescription\", \"count\" : 2 }", "name is missing"},
                {"{ \"name\" : \"\", \"description\" : \"ItemDescription\", \"count\" : 2 }", "name can't be empty"},
                {"{ \"name\" : \"!name\", \"description\" : \"ItemDescription\", \"count\" : 2 }", "name have to be alpha-numeric"},
                {"{ \"name\" : \"namenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamename\", \"description\" : \"ItemDescription\", \"count\" : 2 }", "name must not exceed 50 characters"},
                {"{ \"name\" : \"nameItem\", \"descripti\" : \"ItemDescription\", \"count\" : 2 }", "description is missing"},
                {"{ \"name\" : \"nameItem\", \"description\" : \"\", \"count\" : 2 }", "description can't be empty"},
                {"{ \"name\" : \"nameItem\", \"description\" : \"Item!Description\", \"count\" : 2 }", "description have to be alpha-numeric"},
                {"{ \"name\" : \"nameItem\", \"description\" : \"ItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescriptionItemDescription\", \"count\" : 2 }", "description must not exceed 150 characters"},
                {"{ \"name\" : \"nameItem\", \"description\" : \"ItemDescription\", \"count\" : 0 }", "count must be between 1 and 100"},
                {"{ \"name\" : \"nameItem\", \"description\" : \"ItemDescription\", \"count\" : 101 }", "count must be between 1 and 100"},
                {"{ \"name\" : \"nameItem\", \"description\" : \"ItemDescription\", \"count\" : 1, \"updatedAt\": \"2012-01-01T10:10:11\"}", "setting updatedAt manually is not allowed"},
        };

        for (Object[] testRecord: testData) {
            testValidationMessage((String)testRecord[0], (String)testRecord[1]);
        }
    }

    public void testValidationMessage(String json, String message) throws Exception{
        mockMvc.perform(post("/items").contentType(contentType).content(json))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors[0].message", is(message)));
    }
}